package com.jacky.catlauncher.util;

import com.jacky.catlauncher.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by yang on 19/04/17.
 */

public class AppTypeList {

    private static final HashMap<String,Integer> iptvPackageMap = new HashMap<>();
    private static final HashMap<String,Integer> musicPackageMap = new HashMap<>();
    public static final String googleplayPackage = "com.android.vending";
    public static final int googleplayBackgroundId = R.drawable.googleplay;
    public static final String settingsPackage = "com.android.tv.settings";
    public static final int settingsBackgroundId = R.drawable.settings;


    static {
        iptvPackageMap.put("com.google.android.youtube.tv", R.drawable.youtube);
        iptvPackageMap.put("com.netflix.ninja", R.drawable.netflix);
        iptvPackageMap.put("org.xbmc.kodi", R.drawable.kodi);
        iptvPackageMap.put("au.com.seebo.iptv", R.drawable.iivictor);
        musicPackageMap.put("com.spotify.tv.android",R.drawable.spotify);
    }

    public static boolean isIPTVPackage(String packagename){
        return iptvPackageMap.containsKey(packagename);
    }

    public static boolean isMusicPackage(String packagename){
        return musicPackageMap.containsKey(packagename);
    }

    public static int getMusicResId(String packagename){
        if(isMusicPackage(packagename)){
            return musicPackageMap.get(packagename);
        }
        return 0;
    }

    public static int getIPTVResId(String packagename){
        if(isIPTVPackage(packagename)){
            return iptvPackageMap.get(packagename);
        }
        return 0;
    }



}
