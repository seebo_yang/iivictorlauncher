package com.jacky.catlauncher.util;

/**
 * Created by yang on 20/04/17.
 */

public class CountryCode {
    private static final String[] countryCodes = new String[]{
        "AI","AG","AW","BS","BB","BZ","BM","BQ","VG","CA","KY",
            "CR","CU","CW","DM","DO","SV","GL","GD","GP","GT","HT",
            "HN","JM","MQ","MX","PM","MS","CW","KN","NI","PA","PR",
            "SX","LC","PM","VC","TT","TC","US","VI"
    };

    public static boolean isNACountryCode(String countryCode){
        for(String s : countryCodes){
            if(countryCode.equals(s)){
                return true;
            }
        }
        return false;
    }

}
