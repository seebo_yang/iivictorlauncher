
package com.jacky.catlauncher.main;

import android.app.Activity;
import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v17.leanback.app.BackgroundManager;
import android.support.v17.leanback.app.BrowseFragment;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ImageCardView;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.OnItemViewClickedListener;
import android.support.v17.leanback.widget.OnItemViewSelectedListener;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.Row;
import android.support.v17.leanback.widget.RowPresenter;
import android.support.v4.app.ActivityOptionsCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.TextClock;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.example.timezonedblibrary.controller.ExternalIPGrabber;
import com.example.timezonedblibrary.models.GEOIP;
import com.jacky.catlauncher.R;
import com.jacky.catlauncher.app.AppCardPresenter;
import com.jacky.catlauncher.app.AppDataManage;
import com.jacky.catlauncher.app.AppModel;
import com.jacky.catlauncher.app.IPTVAppModel;
import com.jacky.catlauncher.detail.MediaDetailsActivity;
import com.jacky.catlauncher.detail.MediaModel;
import com.jacky.catlauncher.function.FunctionCardPresenter;
import com.jacky.catlauncher.function.FunctionModel;
import com.jacky.catlauncher.presenter.MultipleListRow;
import com.jacky.catlauncher.presenter.MultipleListRowPresenter;
import com.jacky.catlauncher.util.CountryCode;
import com.jacky.catlauncher.util.SharedPrefsUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends Activity {

    protected BrowseFragment mBrowseFragment;
    private ArrayObjectAdapter rowsAdapter;
    private BackgroundManager mBackgroundManager;
    private DisplayMetrics mMetrics;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;
        mBrowseFragment = (BrowseFragment) getFragmentManager().findFragmentById(R.id.browse_fragment);

        mBrowseFragment.setHeadersState(BrowseFragment.HEADERS_DISABLED);
        prepareBackgroundManager();
        buildRowsAdapter();
    }

    private void prepareBackgroundManager() {
        mBackgroundManager = BackgroundManager.getInstance(this);
        mBackgroundManager.attach(this.getWindow());
        mMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(mMetrics);
    }

    private void buildRowsAdapter() {
        rowsAdapter = new ArrayObjectAdapter(new MultipleListRowPresenter());

        addIPTVAppRow();
        addMusicAppRow();

        addStoreAppRow();
        addSettingAppRow();
//        addVideoRow();
        addAppRow();


        mBrowseFragment.setAdapter(rowsAdapter);
        mBrowseFragment.setOnItemViewClickedListener(new OnItemViewClickedListener() {
            @Override
            public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item, RowPresenter.ViewHolder rowViewHolder, Row row) {
                if (item instanceof MediaModel) {
                    MediaModel mediaModel = (MediaModel) item;
                    Intent intent = new Intent(mContext, MediaDetailsActivity.class);
                    intent.putExtra(MediaDetailsActivity.MEDIA, mediaModel);

                    Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(
                            (Activity) mContext,
                            ((ImageCardView) itemViewHolder.view).getMainImageView(),
                            MediaDetailsActivity.SHARED_ELEMENT_NAME).toBundle();
                    startActivity(intent, bundle);
                } else if (item instanceof AppModel) {
                    AppModel appBean = (AppModel) item;
                    Intent launchIntent = mContext.getPackageManager().getLaunchIntentForPackage(
                            appBean.getPackageName());
                    if (launchIntent != null) {
                        mContext.startActivity(launchIntent);
                    }
                } else if (item instanceof FunctionModel) {
                    FunctionModel functionModel = (FunctionModel) item;
                    Intent intent = functionModel.getIntent();
                    if (intent != null) {
                        startActivity(intent);
                    }
                }
            }
        });
        mBrowseFragment.setOnItemViewSelectedListener(new OnItemViewSelectedListener() {
            @Override
            public void onItemSelected(Presenter.ViewHolder itemViewHolder, Object item, RowPresenter.ViewHolder rowViewHolder, Row row) {

            }
        });
    }

    private void addIPTVAppRow() {
        String headerName = getResources().getString(R.string.app_header_iptv_name);
        ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(new AppCardPresenter());

        ArrayList<IPTVAppModel> appDataList = new AppDataManage(mContext).getIPTVAppList();
        int cardCount = appDataList.size();

        for (int i = 0; i < cardCount; i++) {
            if(i > 0 && appDataList.get(i).getPackageName().equals("au.com.seebo.iptv")){
                listRowAdapter.add(0,appDataList.get(i));
                continue;
            }
            listRowAdapter.add(appDataList.get(i));
        }
        HeaderItem header = new HeaderItem(0, headerName);
        MultipleListRow row = new MultipleListRow(header, listRowAdapter);
        int numRows = cardCount % 6 == 0 ? cardCount / 6 : cardCount / 6 + 1;
        row.setNumRows(numRows);
        rowsAdapter.add(row);
    }

    private void addMusicAppRow() {
        String headerName = getResources().getString(R.string.app_header_music_name);
        ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(new AppCardPresenter());

        ArrayList<AppModel> appDataList = new AppDataManage(mContext).getMusicAppList();
        int cardCount = appDataList.size();

        for (int i = 0; i < cardCount; i++) {
            listRowAdapter.add(appDataList.get(i));
        }
        HeaderItem header = new HeaderItem(0, headerName);
        MultipleListRow row = new MultipleListRow(header, listRowAdapter);
        int numRows = cardCount % 6 == 0 ? cardCount / 6 : cardCount / 6 + 1;
        row.setNumRows(numRows);
        rowsAdapter.add(row);
    }

    private void addStoreAppRow() {
        String headerName = getResources().getString(R.string.app_header_store_name);
        ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(new AppCardPresenter());

        ArrayList<AppModel> appDataList = new AppDataManage(mContext).getStores();
        int cardCount = appDataList.size();

        for (int i = 0; i < cardCount; i++) {
            listRowAdapter.add(appDataList.get(i));
        }
        HeaderItem header = new HeaderItem(0, headerName);
        MultipleListRow row = new MultipleListRow(header, listRowAdapter);
        int numRows = cardCount % 6 == 0 ? cardCount / 6 : cardCount / 6 + 1;
        row.setNumRows(numRows);
        rowsAdapter.add(row);
    }

    private void addSettingAppRow() {
        String headerName = getResources().getString(R.string.app_header_setting_name);
        ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(new AppCardPresenter());

        ArrayList<AppModel> appDataList = new AppDataManage(mContext).getSetting();
        int cardCount = appDataList.size();

        for (int i = 0; i < cardCount; i++) {
            listRowAdapter.add(appDataList.get(i));
        }
        HeaderItem header = new HeaderItem(0, headerName);
        MultipleListRow row = new MultipleListRow(header, listRowAdapter);
        int numRows = cardCount % 6 == 0 ? cardCount / 6 : cardCount / 6 + 1;
        row.setNumRows(numRows);
        rowsAdapter.add(row);
    }

    private void addVideoRow() {
        String headerName = getResources().getString(R.string.app_header_video_name);
        ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(new ImgCardPresenter());
        for (MediaModel mediaModel : MediaModel.getVideoModels()) {
            listRowAdapter.add(mediaModel);
        }
        HeaderItem header = new HeaderItem(0, headerName);
        rowsAdapter.add(new ListRow(header, listRowAdapter));
    }

    private void addAppRow() {
        String headerName = getResources().getString(R.string.app_header_app_name);
        ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(new AppCardPresenter());

        ArrayList<AppModel> appDataList = new AppDataManage(mContext).getLaunchAppList();
        int cardCount = appDataList.size();

        for (int i = 0; i < cardCount; i++) {
            if(appDataList.get(i).getPackageName().contains("catlauncher")){
                continue;
            }
            listRowAdapter.add(appDataList.get(i));
        }
        HeaderItem header = new HeaderItem(0, headerName);
        MultipleListRow row = new MultipleListRow(header, listRowAdapter);
        int numRows = cardCount % 6 == 0 ? cardCount / 6 : cardCount / 6 + 1;
        row.setNumRows(3);
        rowsAdapter.add(row);
    }

    private void addFunctionRow() {
        String headerName = getResources().getString(R.string.app_header_function_name);
        ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(new FunctionCardPresenter());
        List<FunctionModel> functionModels = FunctionModel.getFunctionList(mContext);
        int cardCount = functionModels.size();
        for (int i = 0; i < cardCount; i++) {
            listRowAdapter.add(functionModels.get(i));
        }
        HeaderItem header = new HeaderItem(0, headerName);
        rowsAdapter.add(new ListRow(header, listRowAdapter));
    }

    @Override
    protected void onResume() {
        checkRunningCountry();
        mBackgroundManager.setDrawable(getDrawable(R.drawable.iivictor));
        super.onResume();
    }

    public class NetworkConnectionReceiver extends BroadcastReceiver {


        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)){
                checkRunningCountry();
            }

        }
    }

    private void checkRunningCountry(){
        ExternalIPGrabber externalIPGrabber= new ExternalIPGrabber();
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo == null){
            return;
        }
        if(networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED){


            externalIPGrabber.getIP(new Callback<GEOIP>() {
                @Override
                public void onResponse(Call<GEOIP> call, Response<GEOIP> response) {
                    GEOIP geoIP = response.body();
                    if (geoIP == null) {
                        return;
                    }
                    String zipCode = geoIP.getZip_code();
                    String countryCode = geoIP.getCountry_code();
                    AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    am.setTimeZone(geoIP.getTime_zone());
                    Log.d("!!!!!!",countryCode);
//                                SharedPrefsUtils.setStringPreference(context, TZ_KEY, geoIP.getTime_zone());
//                                SharedPrefsUtils.setStringPreference(context, ZIP_KEY,zipCode);
//                                SharedPrefsUtils.setStringPreference(context, COUNTRY_KEY,countryCode);
                    if(CountryCode.isNACountryCode(countryCode)){
                        showBlockMessage();
                    }
                }

                @Override
                public void onFailure(Call<GEOIP> call, Throwable t) {
                    showBlockMessage();
                }
            });

        }
    }

    private void showBlockMessage(){
        WarningDialogFragment warningDialogFragment = new WarningDialogFragment();
        warningDialogFragment.show(getFragmentManager(),"WARNING");
        warningDialogFragment.setCancelable(false);
    }

    @Override
    public void onBackPressed() {
    }
}
