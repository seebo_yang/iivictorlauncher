package com.jacky.catlauncher.main;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;

import com.jacky.catlauncher.R;

/**
 * Created by yang on 24/03/17.
 */

public class WarningDialogFragment extends DialogFragment {

    private OnWarningListener mListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        if(!(getActivity() instanceof OnWarningListener)){
//            throw new IllegalStateException("Activity using this must implment OnWarningListener");
//        }
//        mListener = (OnWarningListener)getActivity();
        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setCancelable(false)
                .setView(LayoutInflater.from(getActivity()).inflate(R.layout.fragment_warning_dialog,null))
                .create();

        return dialog;
    }

    public interface OnWarningListener{
        void onOKPressed();
    }



}
