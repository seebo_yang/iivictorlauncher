package com.jacky.catlauncher.presenter;

import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ObjectAdapter;

/**
 * Created by yang on 12/05/17.
 */

public class MultipleListRow extends ListRow {

    private static final String TAG = MultipleListRow.class.getSimpleName();

    private int mNumRows = 1;

    public MultipleListRow(HeaderItem header, ObjectAdapter adapter) {
        super(header, adapter);
    }

    public MultipleListRow(long id, HeaderItem header, ObjectAdapter adapter) {
        super(id, header, adapter);
    }

    public MultipleListRow(ObjectAdapter adapter) {
        super(adapter);
    }

    public void setNumRows(int numRows) {
        mNumRows = numRows;
    }

    public int getNumRows() {
        return mNumRows;
    }
}
