package com.jacky.catlauncher.presenter;


import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.RowHeaderPresenter;
import android.view.ViewGroup;

/**
 * Created by yang on 12/05/17.
 */

public class IconHeaderItemPresenter extends RowHeaderPresenter {

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {

        return super.onCreateViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        super.onBindViewHolder(viewHolder, item);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
        super.onUnbindViewHolder(viewHolder);
    }
}
