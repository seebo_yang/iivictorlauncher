package com.jacky.catlauncher.presenter;

import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.RowPresenter;

/**
 * Created by yang on 12/05/17.
 */

public class MultipleListRowPresenter extends ListRowPresenter {

    private static final String TAG = MultipleListRowPresenter.class.getSimpleName();

    @Override
    protected void onBindRowViewHolder(RowPresenter.ViewHolder holder, Object item) {
        int  numRows = ((MultipleListRow) item).getNumRows();
        ((ListRowPresenter.ViewHolder)holder).getGridView().setNumRows(numRows);
        ((ListRowPresenter.ViewHolder)holder).getGridView().setFadingRightEdge(true);
        super.onBindRowViewHolder(holder, item);
    }

    @Override
    protected void initializeRowViewHolder(RowPresenter.ViewHolder holder) {
        super.initializeRowViewHolder(holder);
    }
}
