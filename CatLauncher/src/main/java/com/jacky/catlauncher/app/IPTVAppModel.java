package com.jacky.catlauncher.app;

/**
 * Created by yang on 19/04/17.
 */

public class IPTVAppModel extends AppModel {

    private int drawableId;

    public IPTVAppModel(AppModel appModel, int drawableId){
        this.setDataDir(appModel.getDataDir());
        this.setIcon(appModel.getIcon());
        this.setId(appModel.getId());
        this.setLauncherName(appModel.getLauncherName());
        this.setName(appModel.getName());
        this.setPackageName(appModel.getPackageName());
        this.setPageIndex(appModel.getPageIndex());
        this.setPosition(appModel.getPosition());
        this.setSysApp(appModel.isSysApp());
        this.drawableId = drawableId;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }
}
