
package com.jacky.catlauncher.app;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;

import com.jacky.catlauncher.util.AppTypeList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AppDataManage {

    private final Context mContext;

    public AppDataManage(Context context) {
        mContext = context;
    }

    public ArrayList<AppModel> getLaunchAppList() {
        PackageManager localPackageManager = mContext.getPackageManager();
        Intent localIntent = new Intent("android.intent.action.MAIN");
        localIntent.addCategory("android.intent.category.LAUNCHER");
        List<ResolveInfo> localList = localPackageManager.queryIntentActivities(localIntent, 0);
        ArrayList<AppModel> localArrayList = null;
        Iterator<ResolveInfo> localIterator = null;
        localArrayList = new ArrayList<>();
        if (localList.size() != 0) {
            localIterator = localList.iterator();
        }
        while (true) {
            if (!localIterator.hasNext())
                break;
            ResolveInfo localResolveInfo = (ResolveInfo) localIterator.next();
            AppModel localAppBean = new AppModel();
            localAppBean.setIcon(localResolveInfo.activityInfo.loadIcon(localPackageManager));
            localAppBean.setName(localResolveInfo.activityInfo.loadLabel(localPackageManager).toString());
            localAppBean.setPackageName(localResolveInfo.activityInfo.packageName);
            localAppBean.setDataDir(localResolveInfo.activityInfo.applicationInfo.publicSourceDir);
            localAppBean.setLauncherName(localResolveInfo.activityInfo.name);
            String pkgName = localResolveInfo.activityInfo.packageName;
            PackageInfo mPackageInfo;
            try {
                mPackageInfo = mContext.getPackageManager().getPackageInfo(pkgName, 0);
                if ((mPackageInfo.applicationInfo.flags & mPackageInfo.applicationInfo.FLAG_SYSTEM) > 0) {
                    localAppBean.setSysApp(true);
                }
            } catch (NameNotFoundException e) {
                e.printStackTrace();
            }
            if (!localAppBean.getPackageName().equals("com.jacky.launcher")) {
                localArrayList.add(localAppBean);
            }
        }
        return localArrayList;
    }


    public ArrayList<IPTVAppModel> getIPTVAppList(){
        ArrayList<IPTVAppModel> iptvAppModels = new ArrayList<>();
        ArrayList<AppModel> tempList = getLaunchAppList();
        for(AppModel appModel : tempList){
            if(AppTypeList.isIPTVPackage(appModel.getPackageName())){
                IPTVAppModel iptvAppModel = new IPTVAppModel(appModel,AppTypeList.getIPTVResId(appModel.getPackageName()));
                iptvAppModels.add(iptvAppModel);
            }
        }
        return iptvAppModels;
    }

    public ArrayList<AppModel> getMusicAppList(){
        ArrayList<AppModel> musicAppModels = new ArrayList<>();
        ArrayList<AppModel> appModels = getLaunchAppList();
        for(AppModel appModel : appModels){
            if(AppTypeList.isMusicPackage(appModel.getPackageName())){
//                if(AppTypeList.getMusicResId(appModel.getPackageName()) != 0) {
//                    appModel.setBackground(mContext.getDrawable(AppTypeList.getMusicResId(appModel.getPackageName())));
//                }
                musicAppModels.add(appModel);
            }
        }
        return musicAppModels;
    }

    public ArrayList<AppModel> getSetting(){
        ArrayList<AppModel> settingAppModels = new ArrayList<>();
        ArrayList<AppModel> appModels = getLaunchAppList();
        for(AppModel appModel : appModels){
            if(appModel.getPackageName().equals(AppTypeList.settingsPackage)){
//                if(AppTypeList.settingsBackgroundId != 0) {
//                    appModel.setBackground(mContext.getDrawable(AppTypeList.settingsBackgroundId));
//                }
                settingAppModels.add(appModel);
                return settingAppModels;
            }
        }
        return settingAppModels;
    }

    public ArrayList<AppModel> getStores(){
        ArrayList<AppModel> storeAppModels = new ArrayList<>();
        ArrayList<AppModel> appModels = getLaunchAppList();
        for(AppModel appModel : appModels){
            if(appModel.getPackageName().equals(AppTypeList.googleplayPackage)){
//                if(AppTypeList.googleplayBackgroundId != 0) {
//                    appModel.setBackground(mContext.getDrawable(AppTypeList.googleplayBackgroundId));
//                }
                storeAppModels.add(appModel);
                return storeAppModels;
            }
        }
        return storeAppModels;
    }







    public ArrayList<AppModel> getUninstallAppList() {
        PackageManager localPackageManager = mContext.getPackageManager();
        Intent localIntent = new Intent("android.intent.action.MAIN");
        localIntent.addCategory("android.intent.category.LAUNCHER");
        List<ResolveInfo> localList = localPackageManager.queryIntentActivities(localIntent, 0);
        ArrayList<AppModel> localArrayList = null;
        Iterator<ResolveInfo> localIterator = null;
        if (localList != null) {
            localArrayList = new ArrayList<>();
            localIterator = localList.iterator();
        }
        while (true) {
            if (!localIterator.hasNext())
                break;
            ResolveInfo localResolveInfo = (ResolveInfo) localIterator.next();
            AppModel localAppBean = new AppModel();
            localAppBean.setIcon(localResolveInfo.activityInfo.loadIcon(localPackageManager));
            localAppBean.setName(localResolveInfo.activityInfo.loadLabel(localPackageManager).toString());
            localAppBean.setPackageName(localResolveInfo.activityInfo.packageName);
            localAppBean.setDataDir(localResolveInfo.activityInfo.applicationInfo.publicSourceDir);
            String pkgName = localResolveInfo.activityInfo.packageName;
            PackageInfo mPackageInfo;
            try {
                mPackageInfo = mContext.getPackageManager().getPackageInfo(pkgName, 0);
                if ((mPackageInfo.applicationInfo.flags & mPackageInfo.applicationInfo.FLAG_SYSTEM) > 0) {// 系统预装
                    localAppBean.setSysApp(true);
                } else {
                    localArrayList.add(localAppBean);
                }
            } catch (NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return localArrayList;
    }

    public ArrayList<AppModel> getAutoRunAppList() {
        PackageManager localPackageManager = mContext.getPackageManager();
        Intent localIntent = new Intent("android.intent.action.MAIN");
        localIntent.addCategory("android.intent.category.LAUNCHER");
        List<ResolveInfo> localList = localPackageManager.queryIntentActivities(localIntent, 0);
        ArrayList<AppModel> localArrayList = null;
        Iterator<ResolveInfo> localIterator = null;
        if (localList != null) {
            localArrayList = new ArrayList<>();
            localIterator = localList.iterator();
        }

        while (true) {
            if (!localIterator.hasNext())
                break;
            ResolveInfo localResolveInfo = localIterator.next();
            AppModel localAppBean = new AppModel();
            localAppBean.setIcon(localResolveInfo.activityInfo.loadIcon(localPackageManager));
            localAppBean.setName(localResolveInfo.activityInfo.loadLabel(localPackageManager).toString());
            localAppBean.setPackageName(localResolveInfo.activityInfo.packageName);
            localAppBean.setDataDir(localResolveInfo.activityInfo.applicationInfo.publicSourceDir);
            String pkgName = localResolveInfo.activityInfo.packageName;
            String permission = "android.permission.RECEIVE_BOOT_COMPLETED";
            try {
                PackageInfo mPackageInfo = mContext.getPackageManager().getPackageInfo(pkgName, 0);
                if ((PackageManager.PERMISSION_GRANTED == localPackageManager.checkPermission(permission, pkgName))
                        && !((mPackageInfo.applicationInfo.flags & mPackageInfo.applicationInfo.FLAG_SYSTEM) > 0)) {
                    localArrayList.add(localAppBean);
                }
            } catch (NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return localArrayList;
    }
}
