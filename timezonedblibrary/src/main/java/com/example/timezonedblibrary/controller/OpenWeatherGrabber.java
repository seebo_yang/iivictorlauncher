package com.example.timezonedblibrary.controller;

import com.example.timezonedblibrary.interfaces.OpenWeatherService;
import com.example.timezonedblibrary.models.WeatherResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by yang on 20/12/16.
 */

public class OpenWeatherGrabber {

    private static final String baseUrl = "http://api.openweathermap.org/data/2.5/";
    private static final String API_KEY = "61f806dad532bad3654c034c0b524f23";
    private Retrofit retrofit;
    private OpenWeatherService service;

    public OpenWeatherGrabber() {
        retrofit = new Retrofit.Builder().baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(OpenWeatherService.class);
    }

    public void getCurrentWeather(Callback<WeatherResponse> callback, String zipCode, String countryCode) {
        Call<WeatherResponse> call = service.getWeather(API_KEY, "metric", zipCode + "," + countryCode);
        call.enqueue(callback);
    }
}
