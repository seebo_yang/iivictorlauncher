package com.example.timezonedblibrary.interfaces;

import com.example.timezonedblibrary.models.GEOIP;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by yang on 20/12/16.
 */


public interface ExternalIPService {
    @GET("json/")
    Call<GEOIP> getGEOIP();
}
